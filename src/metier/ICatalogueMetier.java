package metier;

import java.util.List;

import metier.entities.Categorie;
import metier.entities.ResponseListeArticles;
import metier.entities.Utilisateur;
import metier.entities.AjoutArticle;
import metier.entities.Article;

public interface ICatalogueMetier {
		
	public Categorie addCategorie(Categorie categorie);
	public Article addArticle(Article article);
	public Article addArticleApi(AjoutArticle a);
	public List<Categorie> listeCategories();
	public ResponseListeArticles articlesParCat(String nomCategorie);
	public List<Article> listArticles();
	public Categorie updateCategorie(Categorie categorie);
	public Article updateArticle(Article article);
	public boolean deleteArticle(Long idArticle);
	public Article getArticle(Long idArticle);
	public Categorie getCategorie(Long idCategorie);
	public Utilisateur getUtilisateur(String email);
	public Utilisateur updateUtilisateur(Utilisateur utilisateur);
	public boolean deleteUtilisateur(String email);
	public Utilisateur addUtilisateur(Utilisateur utilisateur);
	public List<Utilisateur> listUtilisateurs();


}
