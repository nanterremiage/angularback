package metier;

import java.util.*;
import metier.entities.Categorie;
import metier.entities.ResponseListeArticles;
import metier.entities.Utilisateur;
import metier.entities.AjoutArticle;
import metier.entities.Article;

public class CatalogueMetierImpl implements ICatalogueMetier{
	
	private Map<Long, Article> articles= new HashMap<>();
	private Map<Long, Categorie> categories= new HashMap<>();
	private Map<String, Utilisateur> utilisateurs= new HashMap<>();

	@Override
	public Categorie addCategorie(Categorie categorie) {
		return categories.put(categorie.getId(), categorie);
	}

	@Override
	public Article addArticle(Article article) {
		try {
			article.setId(idIncrementation());
		}catch(Exception e) {}
		return articles.put(article.getId(), article);
	}
	
	@Override
	public Article addArticleApi(AjoutArticle a) {
		Article article = a.getArticle();
		Long idC= a.getIdCategorie();
		try {
			article.setCategorie(categories.get(idC));
			article.setId(idIncrementation());
		}catch(Exception e) {}
		return articles.put(article.getId(), article);
	}

	@Override
	public List<Categorie> listeCategories() {
		return new ArrayList<>(categories.values());
	}

	@Override
	public ResponseListeArticles articlesParCat(String nomCategorie) {
		Categorie cat = getCategorie(1L);
		for (Categorie value : categories.values()) {
			if(value.getNom().equalsIgnoreCase(nomCategorie)){
				cat=value;
			}
		}
		List<Categorie> listCat = new ArrayList<Categorie>();
		listCat.add(cat);
		cat.recupTousLesFils(cat, listCat);
		
		List<Article> listeArticles = new ArrayList<>();
		for(Article article: articles.values()) {
			for (Categorie categorie : listCat) {
				if(article.getCategorie().getId().equals(categorie.getId())) {
					listeArticles.add(article);
				}
			}
		}
		return new ResponseListeArticles(listeArticles, cat);
	}
	
	@Override
	public List<Article> listArticles() {
		return new ArrayList<>(articles.values());
	}
	@Override
	public Categorie updateCategorie(Categorie categorie) {
		return categories.put(categorie.getId(), categorie);
	}
	@Override
	public Article updateArticle(Article article) {
		Long id = article.getId();
		Long idC = article.getCategorie().getId();
		article.setCategorie(categories.get(idC));
		return articles.put(id, article);
	}

	@Override
	public boolean deleteArticle(Long idArticle) {
		if(articles.get(idArticle)!=null) {
			articles.remove(idArticle);
			return true;
		}
		return false;
	}

	@Override
	public Article getArticle(Long idArticle) {
		return articles.get(idArticle);
	}

	@Override
	public Categorie getCategorie(Long idCategorie) {
		return categories.get(idCategorie);
		
	}
	
	@Override
	public Utilisateur getUtilisateur(String email) {
		for(Utilisateur u: utilisateurs.values()) {
			if(u.getEmail().equals(email)) {
				return u;
			}
		}
		return null;
	}
	@Override
	public Utilisateur addUtilisateur(Utilisateur utilisateur) {
		return utilisateurs.put(utilisateur.getEmail(), utilisateur);
	}

	@Override
	public Utilisateur updateUtilisateur(Utilisateur utilisateur) {
		return utilisateurs.put(utilisateur.getEmail(), utilisateur);
	}

	@Override
	public boolean deleteUtilisateur(String email) {
		if(utilisateurs.get(email)!=null) {
			utilisateurs.remove(email);
			return true;
		}
		return false;
	}

	@Override
	public List<Utilisateur> listUtilisateurs() {
		return new ArrayList<>(utilisateurs.values());
	}
	
	public Long idIncrementation () {
		List<Long> listId= new ArrayList<>(articles.keySet());
		if(listId !=null) {
			return Collections.max(listId)+1L;
		}
		return 1L;
	}
	
	public void instance() {
		
		 Categorie categorie1 = new Categorie(1L,"PC", "Pc", null);
		 Categorie categorie2 = new Categorie(2L,"PCBureautique", "Ordinateurs bureautique", null);
		 Categorie categorie3 = new Categorie(3L,"PCPortable", "Ordinateurs portables", null);
		 Categorie categorie4 = new Categorie(4L,"PCAccessoires", "Accessoire d'ordinateur", null);
		 Categorie categorie5 = new Categorie(5L,"Telephone", "T�l�phones", null);
		 Categorie categorie6 = new Categorie(6L,"Smartphone", "T�l�phones portables", null);
		 Categorie categorie7 = new Categorie(7L,"TelephoneFixe", "T�l�phones fixe", null);
		 Categorie categorie8 = new Categorie(8L,"TelephoneAccessoires", "Accessoires de t�l�phones", null);
		 Categorie categorie9 = new Categorie(9L,"Stockage", "Stockages", null);
		 Categorie categorie10 = new Categorie(10L,"CleUsb", "Cl�s USB", null);
		 Categorie categorie11 = new Categorie(11L,"DisqueDur","Disques durs", null);
		 Categorie categorie12 = new Categorie(12L,"StockageAccessoires","Accessoires de stockage", null);
		 categorie1.setsousCategorie(new ArrayList<>(Arrays.asList(categorie2,categorie3,categorie4)));
		 categorie5.setsousCategorie(new ArrayList<>(Arrays.asList(categorie6,categorie7,categorie8)));
		 categorie9.setsousCategorie(new ArrayList<>(Arrays.asList(categorie10,categorie11,categorie12)));
		 
		 addCategorie(categorie1);
		 addCategorie(categorie2);
		 addCategorie(categorie3);
		 addCategorie(categorie4);
		 addCategorie(categorie5);
		 addCategorie(categorie6);
		 addCategorie(categorie7);
		 addCategorie(categorie8);
		 addCategorie(categorie9);
		 addCategorie(categorie10);
		 addCategorie(categorie11);
		 addCategorie(categorie12);
		 
		//PCBUREAUTIQUE id = 2
			addArticle(new Article(20004L, "Ankermann Silent Office Business PC", "Ankermann",399.00, getCategorie(2L), "https://images-na.ssl-images-amazon.com/images/I/61O3cni5B-L._AC_SX425_.jpg"));
			addArticle(new Article(20005L, "PC Gamer 10-Core (4C+6G)", "Shinobee",366.00, getCategorie(2L), "https://images-na.ssl-images-amazon.com/images/I/41Z8FWiultL._AC_.jpg"));
			addArticle(new Article(20006L, "Dell PC 7010 SFF Ecran 22", "Dell",364.95, getCategorie(2L), "https://images-na.ssl-images-amazon.com/images/I/41I-f3ddk3L._AC_.jpg"));
			addArticle(new Article(20007L, "Ordinateur de Bureau Tout-en-Un NEXSMART", "NEXSMART",605.00, getCategorie(2L), "https://images-na.ssl-images-amazon.com/images/I/71oOdyT2sEL._AC_SL1200_.jpg"));

			//PC PORTABLE : id = 3
		    addArticle(new Article(30001L, "MacBook Air 2020", "Apple",1129.00, getCategorie(3L), "https://www.apple.com/newsroom/images/tile-images/Apple_new-macbook-air-wallpaper-screen_03182020.jpg.landing-big_2x.jpg"));
		    addArticle(new Article(30002L, "MacBook Pro 2020", "Apple",1449.00, getCategorie(3L), "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp-spacegray-select-202011_GEO_EMEA_LANG_FR?wid=892&hei=820&&qlt=80&.v=1613672865000"));
			addArticle(new Article(30004L, "Asus ROG Zephyrus G14", "Asus",1399.99, getCategorie(3L), "https://c0.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2020/04/asus-rog-zephyrus-g14-frandroid-2020.png?resize=580,580"));
			addArticle(new Article(30005L, "Inspiron 15-3000", "Dell",529.04, getCategorie(3L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/dell_client_products/notebooks/inspiron_notebooks/inspiron_3505/pdp/notebook_laptop_inspiron_bullseye_bk_amd_pdp_gallery.jpg?fmt=jpg&wid=570&hei=400"));
			addArticle(new Article(30006L, "Inspiron 13-5000", "Dell",699.04, getCategorie(3L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/dell_client_products/notebooks/inspiron_notebooks/inspiron_5300/global_spi/gray/ng/notebook-inspiron-13-5300-silver-graykb-campaign-hero-504x350-ng.psd?fmt=jpg&wid=570&hei=400"));
			addArticle(new Article(30007L, "Inspiron 15-7000 2in1", "Dell",849.05, getCategorie(3L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/dell_client_products/notebooks/inspiron_notebooks/15_7500/global_spi/ng/notebook-inspiron-15-7500-silver-n7-icl-campaign-hero-504x350-ng.psd?fmt=jpg&wid=570&hei=400"));
			addArticle(new Article(30008L, "Inspiron 14-7000", "Dell",879.04, getCategorie(3L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/dell_client_products/notebooks/inspiron_notebooks/14_7400/global_spi/ng/notebook-inspiron-14-7400-campaign-hero-504x350-ng.psd?fmt=jpg&wid=570&hei=400"));
			addArticle(new Article(30009L, "HP 15s-fq2012nf", "HP",799.00, getCategorie(3L), "https://store.hp.com/FranceStore/Html/Merch/Images/c06427625_209x189.jpg"));
			addArticle(new Article(30010L, "HP Spectre x360 14-ea0000nf", "HP",2299.00, getCategorie(3L), "https://store.hp.com/FranceStore/Html/Merch/Images/2V9J9EA-ABF_6_209x189.jpg"));
			
			//PC ACCESSOIRES : id = 4
		    addArticle(new Article(40001L, "Station d�accueil universelle Dell D6000", "Dell",209.47, getCategorie(4L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/peripherals/expansion/dell/docks_and_stands/d6000/hero/dell-universal-dock-d6000-hero-504x350.jpg?fmt=jpg"));
		    addArticle(new Article(40002L, "Clavier multim�dia Dell - KB216 - fran�ais (AZERTY)", "Dell",18.89, getCategorie(4L), "https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/peripherals/input_devices/dell/keyboards/kb216/hero/dell-multimedia-keyboard-kb216-hero-504x350.jpg?fmt=png-alpha"));
		    addArticle(new Article(40003L, "Souris Bluetooth Dell WM615", "Dell",56.65, getCategorie(4L), "https://i.dell.com/das/xa.ashx/global-site-design%20WEB/7de9d3e9-a56c-bde1-3f07-d113aad0b90f/1/OriginalPng?id=Dell/Product_Images/Peripherals/Input_Devices/Dell/Mouse/wm615/dell-wm615-bluetooth-mouse-hero-504x350.psd"));
		    addArticle(new Article(40004L, "Sac � chargement par le haut HP Renew, 15 pouces", "HP",69.98, getCategorie(4L), "https://store.hp.com/FranceStore/Html/Merch/Images/c06658503_209x189.jpg"));
		    addArticle(new Article(40005L, "Sac � dos classique HP de 39,62 cm (15,6 pouces)", "HP",19.99, getCategorie(4L), "https://store.hp.com/FranceStore/Html/Merch/Images/c05408829_209x189.jpg"));

			//T�l�phone : id = 5 (Cat�gorie Parent donc pas d'article dedans)
			
			//smartphone : id = 6
		    addArticle(new Article(60001L, "iPhone 12 64 giga", "Apple",909.00, getCategorie(6L), "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-family-select-2020?wid=940&amp;hei=1112&amp;fmt=jpeg&amp;qlt=80&amp;.v=1604343709000"));
		    addArticle(new Article(60002L, "iPhone 12 pro 128 go", "Apple",1159.00, getCategorie(6L), "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-pro-blue-hero?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1604021661000"));
		    addArticle(new Article(60003L, "iPhone 12 pro Max 128 go", "Apple",1259.00, getCategorie(6L), "https://images.frandroid.com/wp-content/uploads/2020/10/iphone-12-pro-max-frandroid-2020.png"));
		    addArticle(new Article(60004L, "Samsung S21 Ultra 256 go", "Samsung",1309.00, getCategorie(6L), "https://images.samsung.com/is/image/samsung/p6pim/fr/galaxy-s21/gallery/fr-galaxy-s21-ultra-5g-g988-sm-g998bzsgeuh-369048329?$720_576_PNG$"));
		    addArticle(new Article(60005L, "Samsung S21 + 128 go", "Samsung",1059.00, getCategorie(6L), "https://geeko.lesoir.be/wp-content/uploads/sites/58/2020/12/galaxy-s21-1024x690.jpg"));
		    addArticle(new Article(60006L, "Samsung S21 128 go", "Samsung",959.00, getCategorie(6L), "https://c2.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2020/12/samsung-galaxy-s21-frandroid-2021-leak.png?resize=580,580"));
		    addArticle(new Article(60007L, "Vivo X51 5G", "Vivo",599.00, getCategorie(6L), "https://c2.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2020/10/vivo-x51-frandroid-2020-officiel.png?resize=580,580"));
		    addArticle(new Article(60008L, "Xiaomi Mi 11 5G", "Xiaomi",799.00, getCategorie(6L), "https://c2.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2021/03/xiaomi-mi-11-lite-5g-frandroid-2021.png?resize=580,580"));
		    addArticle(new Article(60009L, "Oppo Reno 4", "Oppo",369.00, getCategorie(6L), "https://c2.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2020/09/oppo-reno-4-frandroid-2020.png?resize=580,580"));
		    addArticle(new Article(60010L, "OnePlus 9 Pro", "OnePlus",919.00, getCategorie(6L), "https://c1.lestechnophiles.com/images.frandroid.com/wp-content/uploads/2021/03/oneplus-9-pro-frandroid-2021.png?resize=580,580"));


			//TelephoneFixe : id = 7
			addArticle(new Article(70002L, "T�l�phone fixe sans fil Gigaset AL170A Duo Noir", "Gigaset",48.16, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/CC/CC/8E/7C/8163020-1505-1540-1/tsp20190411191028/Telephone-fixe-sans-fil-Gigaset-AL170A-Duo-Noir.jpg"));
			addArticle(new Article(70003L, "T�l�phone fixe Alcatel E195 Blanc", "Alcatel",19.99, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/e4/bb/bb/12303332/1540-1/tsp20200929170501/Telephone-fixe-Alcatel-E195-Blanc.jpg"));
			addArticle(new Article(70004L, "Panasonic KX-TGA681EXB", "Panasonic",29.99, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/d7/df/3a/3858391/1540-1/tsp20191111130732/Combine-supplementaire-Panasonic-KX-TGA681EXB-pour-KX-TG68.jpg"));
			addArticle(new Article(70005L, "Philips Linea V Solo Blanc", "Philips",50.66, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/f0/62/65/6644464/1540-1/tsp20201210191046/Telephone-fixe-sans-fil-Philips-Linea-V-Solo-Blanc.jpg"));
			addArticle(new Article(70006L, "Logicom Lina 150 Solo Noir", "Logicom",19.99, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/3c/6e/af/11497020/1540-1/tsp20190725103204/Telephone-sans-fil-Logicom-Lina-150-Solo-Noir.jpg"));
			addArticle(new Article(70007L, "Panasonic Dect KX-TG6811 Noir", "Panasonic",34.99, getCategorie(7L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/96/0a/3c/3934870/1540-1/tsp20190830140858/Telephone-fixe-sans-fil-Panasonic-Dect-KX-TG6811-Noir.jpg"));
			

			//Accessoires de t�l�phones : id = 8
			addArticle(new Article(80002L, "C�ble Apple USB-C vers Lightning 1 m", "Apple",29.99, getCategorie(8L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/d1/b7/7b/8107985/1540-1/tsp20200422154420/Cable-Apple-USB-C-vers-Lightning-1-m-Blanc.jpg"));
			addArticle(new Article(80003L, "Support voiture Kenu Airframe Pro", "Airfram",29.99, getCategorie(8L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/4e/a2/57/5743182/1540-1/tsp20201221103032/Support-voiture-Kenu-Airframe-Pro-Noir-pour-Smartphone.jpg"));
			addArticle(new Article(80004L, "Airpods Pro", "Apple",199.00, getCategorie(8L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/75/38/f9/16332917/1540-1/tsp20210331103617/Apple-Airpods-Pro-Blanc-avec-boitier-de-charge-Reconditionne-Grade-A-Reborn.jpg"));
			addArticle(new Article(80005L, "AirPods 2", "Apple",179.00, getCategorie(8L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/14/55/5d/6116628/1540-1/tsp20201130170913/Apple-AirPods-2-avec-boitier-de-charge-Ecouteurs-sans-fil-True-Wirele.jpg"));

			//Stockage : id = 9
			
			//Cl�s USB : id = 10
			addArticle(new Article(100001L, "Cl� USB SanDisk Ultra 3.0, 64 Go", "SanDisk",10.99, getCategorie(10L), "https://static.fnac-static.com/multimedia/Images/FR/NR/02/59/54/5527810/1540-1/tsp20131204130106/Cle-USB-SanDisk-Ultra-3-0-64-Go.jpg"));
			addArticle(new Article(100002L, "Cl� USB Philips Snow 2.0 64 Go Blanc et Violet ", "Philips",12.99, getCategorie(10L), "https://static.fnac-static.com/multimedia/Images/FR/MDM/0a/88/e6/15108106/1540-1/tsp20200603113332/Cle-USB-Philips-Snow-2-0-64-Go-Blanc-et-Violet.jpg"));
			addArticle(new Article(100003L, "Cl� USB 2.0 SanDisk Cruzer Blade, 16 Go, Noire ", "SanDisk",06.99, getCategorie(10L), "https://static.fnac-static.com/multimedia/Images/FR/MC/1e/1c/58/22551582/1540-1/tsp20161221232259/Cle-USB-2-0-SanDisk-Cruzer-Blade-16-Go-Noire.jpg"));
			addArticle(new Article(100004L, "Cl� USB SanDisk Ultra 3.0, 64 Go", "SanDisk",10.99, getCategorie(10L), "https://static.fnac-static.com/multimedia/Images/FR/NR/02/59/54/5527810/1540-1/tsp20131204130106/Cle-USB-SanDisk-Ultra-3-0-64-Go.jpg"));
			addArticle(new Article(100005L, "Cl� USB 1To USB 3.0", "Dongchunxi",29.99, getCategorie(10L), "https://images-na.ssl-images-amazon.com/images/I/617lvPJrhsL._AC_SX425_.jpg"));
			
			//Disques durs : id = 11
			addArticle(new Article(110001L, "Disque dur externe Toshiba HDTB410EK3AA", "Toshiba",45.00, getCategorie(11L), "https://images-na.ssl-images-amazon.com/images/I/910A6B1Sa4L._AC_SX425_.jpg"));
			addArticle(new Article(110002L, "Disque dur �xterne HDD � USB 3.0 Seagate Expansion", "Seagate",114.99, getCategorie(11L), "https://images-na.ssl-images-amazon.com/images/I/91y4lFI%2BuPL._AC_SX679_.jpg"));
			addArticle(new Article(110003L, "Transcend 1To StoreJet 25M3S - 2.5", "Transcend",63.99, getCategorie(11L), "https://images-na.ssl-images-amazon.com/images/I/71%2BccCw9bmL._AC_SX425_.jpg"));
			addArticle(new Article(110004L, "Samsung T7 1 To USB 3.2", "Samsung",165.84, getCategorie(11L), "https://images-na.ssl-images-amazon.com/images/I/91eH8AHvNdL._AC_SX425_.jpg"));
			addArticle(new Article(110005L, "WD - My Passport Ultra 2To", "Western Digital",109.41, getCategorie(11L), "https://images-na.ssl-images-amazon.com/images/I/81hku-x2MWL._AC_SY606_.jpg"));

			
			//Accessoires de stockage : id = 12
			addArticle(new Article(120001L, "Multiprise onduleur 3 prises / cordon IEC C14 noire - 1,5 m", "Dexlan",4.92, getCategorie(12L), "https://accessoires-informatiques.com/images0/3744-1_4/Multiprise-onduleur-3-prises--cordon-IEC-C14-noire--1-5-m.jpg"));
			addArticle(new Article(120002L, "Support Unit� Centrale - sous bureau", "OEM",24.92, getCategorie(12L), "https://accessoires-informatiques.com/images0/3417-1_4/Support-Unite-Centrale--sous-bureau.jpg"));
			addArticle(new Article(120003L, "Adaptateur IEC C14 / CEE7 femelle", "OEM",3.50, getCategorie(12L), "https://accessoires-informatiques.com/images1/35452-1_4/Adaptateur-IEC-C14--CEE7-femelle.jpg"));
			addArticle(new Article(120004L, "Carte son 5.1 PCI Chipset CMedia", "OEM",9.8, getCategorie(12L), "https://accessoires-informatiques.com/images1/42560-1_4/Carte-son-51-PCI-Chipset-CMedia.jpg"));
			addArticle(new Article(120005L, "Mini carte son usb entree/sortie", "DEXLAN",12.42, getCategorie(12L), "https://accessoires-informatiques.com/images0/25055-1_4/Mini-carte-son-usb-entree-sortie.jpg"));
			addArticle(new Article(120006L, "Carte m�re ASUS PRIME AMD X570-P AM4", "ASUS",236.58, getCategorie(12L), "https://accessoires-informatiques.com/images2/59013-1_4/Carte-mere-ASUS-PRIME-AMD-X570-P-AM4.jpg"));
			addArticle(new Article(120007L, "Starter Kit Officiel Raspberry Pi 3", "RASPBERRY",72.42, getCategorie(12L), "https://accessoires-informatiques.com/images1/39503-1_4/Starter-Kit-Officiel-Raspberry-Pi-3.jpg"));
			addArticle(new Article(120008L, "Raspberry Pi 3 Mod�le B+ 1 Go (Cortex A53)", "RASPBERRY",53.25, getCategorie(12L), "https://accessoires-informatiques.com/images1/44649-1_4/Raspberry-Pi-3-Modele-B--1-Go--Cortex-A53.jpg"));
			addArticle(new Article(120009L, "ADVANCE Alimentation ATX 350W NOMINAL VENTIL. ARRIERE 80MM", "ADVANCE",24.08, getCategorie(12L), "https://accessoires-informatiques.com/images1/45167-1_4/ADVANCE-Alimentation-ATX-350W-NOMINAL-VENTIL-ARRIERE-80MM.jpg"));
			addArticle(new Article(120010L, "ADVANCE Bo�tier Moyen tour GRAFIT", "ADVANCE",44.08, getCategorie(12L), "https://accessoires-informatiques.com/images1/36626-1_4/ADVANCE-Boitier-Moyen-tour-GRAFIT.jpg"));

		
		addUtilisateur(new Utilisateur ("user1@utilisateur.com","12345678"));
		addUtilisateur(new Utilisateur ("user2@utilisateur.com","12345678"));
		addUtilisateur(new Utilisateur ("user3@utilisateur.com","12345678"));
		addUtilisateur(new Utilisateur ("user4@utilisateur.com","12345678"));
		addUtilisateur(new Utilisateur ("user5@utilisateur.com","12345678"));
	
	}
}
