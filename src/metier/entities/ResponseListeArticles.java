package metier.entities;

import java.util.List;

public class ResponseListeArticles {
	List<Article> articles;
	Categorie categorie;
	
	
	public ResponseListeArticles(List<Article> articles, Categorie categorie) {
		super();
		this.articles = articles;
		this.categorie = categorie;
	}
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	
}
