package metier.entities;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Categorie {
	private Long id;
	private String nom;
	private List<Categorie> sousCategorie;
	private String nomComplet;
	
	public Categorie(Long id, String nom, String nomComplet, List<Categorie>  sousCategorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.nomComplet = nomComplet;
		this. sousCategorie =  sousCategorie;
	}

	public Categorie() {
		super();
		
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public List<Categorie> getsousCategorie() {
		return sousCategorie;
	}

	public void setsousCategorie(List<Categorie> sousCategorie) {
		this.sousCategorie = sousCategorie;
	}
	
	public Categorie getCatId (Long id) {
		
		for (Categorie c : sousCategorie) {
			if (c.getId().equals(id)) {
				return c;
			}
		}
		return null;
	}
	
	public void recupTousLesFils(Categorie cat, List<Categorie> list){
		if(cat.getsousCategorie()==null) {
			return;
		}
		Iterator<Categorie> iterator = cat.getsousCategorie().iterator();
		while(iterator.hasNext()) {
			Categorie c = iterator.next();
			list.add(c);
			if(c.getsousCategorie()!=null) {
				recupTousLesFils(c,list);
			}	
		}
		return;
	}

	public String getNomComplet() {
		return nomComplet;
	}

	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}	
}
