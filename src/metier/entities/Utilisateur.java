package metier.entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Utilisateur {
	private String email;
	private String password;
	
	
	public Utilisateur() {
		super();
	
	}
	public Utilisateur(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		
		 return email;
	}
	

}
