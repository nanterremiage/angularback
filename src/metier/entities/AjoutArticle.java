package metier.entities;

public class AjoutArticle {
	
	private Article article;
	private Long idCategorie;
	
	public AjoutArticle() {
		super();
	}
	
	public AjoutArticle(Article article, Long idCategorie) {
		super();
		this.article = article;
		this.idCategorie = idCategorie;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Long getIdCategorie() {
		return idCategorie;
	}
	public void setIdCategorie(Long idCategorie) {
		this.idCategorie = idCategorie;
	}
	
	
}
