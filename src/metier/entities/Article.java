package metier.entities;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Article {
	
	private Long id;
	private String libelle;
	private String marque;
	private double prix;
	private Categorie categorie;
	private String photo;
	
	public Article(Long id, String libelle, String marque, double prix, Categorie categorie, String photo ) {
		super();
		this.libelle = libelle;
		this.marque = marque;
		this.prix = prix;
		this.categorie = categorie;
		this.photo = photo;
		this.id = id;
	}

	public Article() {
		super();
	}

	public String getLibelle() {
		return libelle;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
}
