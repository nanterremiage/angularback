package metier.entities;

public class ResponseAjoutArticle {

	private String ajout;

	public ResponseAjoutArticle() {
		super();
	}

	public ResponseAjoutArticle(String ajout) {
		super();
		this.ajout = ajout;
	}

	public String getAjout() {
		return ajout;
	}

	public void setAjout(String ajout) {
		this.ajout = ajout;
	}
	
	
}
