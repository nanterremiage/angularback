package service;

import java.util.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import metier.CatalogueMetierImpl;
import metier.entities.*;


@Path("/boutique")
@Produces({MediaType.APPLICATION_JSON+"; charset=utf-8" })
public class CatalogueService {
	
	private static CatalogueMetierImpl web = new CatalogueMetierImpl();
	static {
		web.instance();
	}
	
	@GET
	@Path("/categories")
	public List<Categorie> getCategorie(){
		return web.listeCategories();
	}
	@GET
	@Path("/articles")
	public List<Article> getArticles(){
		return web.listArticles();
	}
	
	@GET
	@Path("/categorie/{categorie}/articles")
	public ResponseListeArticles getArticlesParCat(@PathParam(value="categorie")String categorie){
		return web.articlesParCat(categorie);
	}
	
	@GET
	@Path("/categorie/{idCategorie}")
	public Categorie categorie(@PathParam(value="idCategorie")Long idCategorie) {
		return web.getCategorie(idCategorie);
	}
	
	@GET
	@Path("/article/{idArticle}")
	public Article article(@PathParam(value="idArticle")Long idArticle) {
		return web.getArticle(idArticle);
	}
	
	@POST
	@Path("/articles")
	public ResponseAjoutArticle addArticleWeb(AjoutArticle article) {
		ResponseAjoutArticle resp= new ResponseAjoutArticle();
		try {
			web.addArticleApi(article);
		}catch(Exception e) {
			resp.setAjout("error");
			return resp;
		}
		resp.setAjout("success");
		return resp; 
	}
	
	@DELETE
	@Path("/article/{idArticle}")
	public boolean deleteArticleWeb(@PathParam(value="idArticle")Long idArticle) {
		return web.deleteArticle(idArticle);
	}
	
	@PUT
	@Path("/articles")
	public ResponseAjoutArticle updateArticleWeb(Article article) {
		ResponseAjoutArticle resp = new ResponseAjoutArticle();
		if(article.getId()==null) {
			resp.setAjout("error");
			return resp;
		}
		web.updateArticle(article);
		resp.setAjout("success");
		return resp;

	}
	
	@GET
	@Path("/utilisateur")
	public Utilisateur getUtilisateurWeb(@QueryParam(value ="email")String email) {
		return web.getUtilisateur(email);
	}
	
	@POST
	@Path("/login")
	public Utilisateur connexion(Utilisateur util) {
		Utilisateur utilisateur = web.getUtilisateur(util.getEmail());
		if(utilisateur!=null) {
			if(utilisateur.getPassword().equals(util.getPassword())) {
				return utilisateur;
			}
		}
		return new Utilisateur();
	}
	
	@POST
	@Path("/utilisateurs")
	public Utilisateur addUtilisateurWeb(Utilisateur utilisateur) {
		return web.addUtilisateur(utilisateur);
	}

	@PUT
	@Path("/utilisateurs")
	public Utilisateur updateUtilisateurWeb(Utilisateur utilisateur) {
		return web.updateUtilisateur(utilisateur);
	}

	@DELETE
	@Path("/utilisateur/{email}")
	public boolean deleteUtilisateur(@QueryParam(value="email")String email) {
		return web.deleteUtilisateur(email);
	}

	@GET
	@Path("/utilisateurs")
	public List<Utilisateur> listUtilisateursWeb() {
		return web.listUtilisateurs();
	}
	
}
